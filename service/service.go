package service

import (
	"gitee.com/go-hutool/hutool-demo/dao"
)

// Service struct
type Service struct {
	dao *dao.Dao
}

// New a DirService and return.
func New() (s *Service) {
	s = &Service{}
	return s
}
