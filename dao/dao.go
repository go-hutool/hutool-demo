package dao

import (
	"gitee.com/go-hutool/hutool-demo/conf"
)

// Dao def
type Dao struct {
}

// New create instance of Dao
func New(c *conf.Config) (d *Dao) {
	d = &Dao{}
	return
}
