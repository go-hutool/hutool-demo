package model

// Strong hello strong.
type Strong struct {
	Hello string
}

type Article struct {
	ID      int64
	Content string
	Author  string
}

// Extended reference
