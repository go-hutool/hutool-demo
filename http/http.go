package http

import (
	"fmt"
	"gitee.com/go-hutool/hutool-demo/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

var (
	svc *service.Service
)

// Init init http sever instance.
func Init(s *service.Service) {
	svc = s
	// init inner router
	router := gin.Default()

	router.GET("/start", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello World")
	})
	fmt.Println("curl http://localhost:8000/start")
	router.Run(":8000")
}
