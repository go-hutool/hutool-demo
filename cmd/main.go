package main

import (
	"gitee.com/go-hutool/hutool-demo/http"
	"gitee.com/go-hutool/hutool-demo/service"
	"gitee.com/go-hutool/hutool/hutool-core/os/signal"
	"gitee.com/go-hutool/hutool/hutool-core/syscall"
	"gitee.com/go-hutool/hutool/hutool-log"
	"os"
	"time"
)

const (
	_durationForClosingServer = 2 // second
)

var (
	svc *service.Service
)

// main $ go run main.go -conf hutool-demo-test.toml
func main() {
	log.Init(nil) // debug flag: log.dir={path}
	defer log.Close()
	log.Info("go hutool start")

	//service init
	svc = service.New()

	//http init
	http.Init(svc)

	log.Info("hutool-demo start")
	signalHandler()
}

// signalHandler 信号
func signalHandler() {
	// init signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	for {
		si := <-c
		log.Info("hutool-demo get a signal %s", si.String())
		switch si {
		case syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGSTOP, syscall.SIGINT:
			//time.Sleep(time.Second * 2)
			log.Info("get a signal %s, stop the hutool-demo process", si.String())
			time.Sleep(_durationForClosingServer * time.Second)
			return
		case syscall.SIGHUP:
		// TODO reload
		default:
			return
		}
	}
}
